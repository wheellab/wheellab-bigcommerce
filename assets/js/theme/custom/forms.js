/* eslint func-names: ["error", "never"] */
/* eslint prefer-arrow-callback: "off" */
/* eslint object-shorthand: "off" */
import PageManager from '../page-manager';
import $ from 'jquery';

export default class forms extends PageManager {
    onReady() {
        this.submitForm();
    }

    submitForm() {
        const form = $('.wheellab-form');
        form.submit(function (e) {
            e.preventDefault();

            // Get form type
            const formType = $('.wheellab-form #form_type').val();
            const formData = form.serializeArray();

            const host = window.location.hostname;
            let endpoint = '';
            if (host === 'localhost') {
                endpoint = 'http://localhost:3001';
            } else {
                endpoint = 'https://api.wheellab.us';
            }

            $.ajax({
                url: `${endpoint}/forms/${formType}`,
                type: 'post',
                data: formData,
                headers: {
                    'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb21wYW55IjoiV2hlZWwgTGFiIExMQyIsImVtYWlsIjoiY29yZXlAd2hlZWxsYWIudXMiLCJpYXQiOjE1NDcyMjMzMTV9.iENQCGco0azEVAjgpen2fIOi9PhPv_nogmTFkH-dCHs',
                },
                dataType: 'json',
                success: function (response) {
                    if (response.success === true) {
                        form.remove();
                        $('.form-response').text(response.message);

                        setTimeout(function () {
                            location.reload();
                        }, 1000);
                    } else {
                        $('.form-response').text(response.message);
                    }
                },
                error: function () {
                    $('.form-response').text('There was a problem submitting the form, please try again later');
                },
            });
        });
    }
}
